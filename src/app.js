import express from 'express';
import MainService from './main.js';


const app = express();

new MainService();

app.listen(3002, () => {
    console.log("Coffee machine ready to use.");
});