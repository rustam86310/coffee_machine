import inquirer from "inquirer";

class MainService {

    isUser = false;
    totalQty = 0;

    constructor() {
        this.initialQues();
    }

    initialQues() {
        const question = {
            type: "input",
            name: "question",
            message: "Please let us know who are you?"
        }
        this.askQuestion(question, true);
    }

    validateNumber(num) {
        if (!isNaN(num)) {
            return true;
        }
        return false
    }

    async terminateProcess() {
        const questionE = {
            type: "input",
            name: "quesE",
            message: "If you want to terminate process press 5 or press 6 for continue."
        }
        let quesE = await this.askQuestion(questionE);
        if (+quesE.quesE === 5) {
            process.exit(-1);
        } else if (+quesE.quesE === 6) {
            this.processToCoffee();
        } else {
            console.log("Thank you for using Coffee machine.");
        }
    }

    async processToCoffee() {
        try {
            if (this.isUser) {
                const question = {
                    type: "input",
                    name: "question",
                    message: "How many coffee's you want?"
                }
                let ques = await this.askQuestion(question);
                let isNum = this.validateNumber(ques.question);
                if (!isNum) {
                    console.log("InValid Quantity.");
                    this.terminateProcess();
                    return;
                }
                if (this.totalQty === 0) {
                    console.log("Sorry, Coffee machine is empty, please contact admin.");
                    this.terminateProcess();
                    return
                }
                if (+ques.question > this.totalQty) {
                    console.log(`Sorry, Quantity not available, you can get ${this.totalQty}.`);
                    const question1 = {
                        type: "input",
                        name: "ques1",
                        message: "If you want to proceed, press 1 or press any key."
                    }
                    let ques1 = await this.askQuestion(question1);
                    if (+ques1.ques1 === 1) {
                        console.log("Please wait, your order is processing.");
                        let timeTakeByCoffeeMachine = +this.totalQty * 1000;
                        await new Promise((resolve, reject) => setTimeout(resolve(), timeTakeByCoffeeMachine));
                        console.log("Your coffee", this.totalQty, "is ready", "Thank you for using Coffee machine.");
                        this.totalQty = 0;
                    }
                } else {
                    console.log("Please wait, your order is processing.");
                    let timeTakeByCoffeeMachine = +ques.question * 1000;
                    await new Promise((resolve, reject) => setTimeout(resolve(), timeTakeByCoffeeMachine));
                    console.log("Your coffee", ques.question, "is ready", "Thank you for using Coffee machine.");
                    this.totalQty = this.totalQty - +ques.question;
                }
                this.terminateProcess();
            } else {
                const question = {
                    type: "input",
                    name: "check-admin",
                    message: "If you want to check Quantity press 1 or press any key"
                }
                let ques = await this.askQuestion(question);
                if (+ques['check-admin'] === 1) {
                    console.log(`Available Quantity: ${this.totalQty}.`);
                }
                const question2 = {
                    type: "input",
                    name: "ques2",
                    message: "If you want to add more Quantity press 1 or press any key"
                }
                let ques2 = await this.askQuestion(question2);
                if (+ques2.ques2 === 1) {
                    const question3 = {
                        type: "input",
                        name: "ques3",
                        message: "Please enter your Quantity"
                    }
                    let ques3 = await this.askQuestion(question3);
                    let isNum = this.validateNumber(ques3.ques3);
                    if (isNum) {
                        this.totalQty = +ques3.ques3;
                        this.initialQues();
                        return
                    } else {
                        console.log("InValid Quantity.");
                    }
                }
                this.terminateProcess();
            }
        } catch (e) {
            console.log(e);
        }
    }

    async askQuestion(question, isFirst) {
        try {
            let question1 = await inquirer.prompt(question);
            if (question1.question === 'user') {
                this.isUser = true;
            }
            if (isFirst) {
                this.processToCoffee();
            }
            return question1;
        } catch (e) {
            console.log(e);
        }
    }
}

export default MainService